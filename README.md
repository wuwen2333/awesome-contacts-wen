# awesome-contacts

> Awesome contacts for expedia test

## Architecture

|    | Framework |
| --- | --- |
| Frontend | Nuxt.js + Element UI |
| Backend | Koa.js + mysql |

## Build Setup

``` bash
# Create env.json by copying env.json.example and set your db configs
{
  "dbHost": "", // mysql db host
  "dbPort": 3306, // mysql db port
  "dbUser": "", // mysql db user
  "dbPassword": "", // mysql db password
  "dbName": "" // mysql db name
}

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Future improvements
For all
  - Research deployment strategy on Cloud
  - Add CICD pipeline
  - Add UTs
  - Add comprehensive error handleing

For frontend
  - Add state management to separate action and view

For backend
  - Optimize database query performance
  - Add api documentation
  - Add log to track anomalies


