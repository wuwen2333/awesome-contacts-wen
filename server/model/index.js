import query from '../db/query';

export const queryContacts = ({
  pageSize,
  pageNumber,
  keywords,
  sortType,
  sortProperty
}) => {
  const limitStart = pageSize * pageNumber;

  return query(`
  SELECT * from contact
  ${keywords ? `WHERE Name LIKE '%${keywords}%'` : ''}
  ${sortProperty ? `ORDER BY ${sortProperty} ${sortType}` : ''}
  LIMIT ${limitStart},${pageSize}
  `)
}

export const queryContactDetails = (userId) => query(`SELECT * from contactdetail WHERE UserID = ${userId}`)

export const queryContactsCount = ({
  keywords,
}) => query(`
  SELECT COUNT(Name) from contact
  ${keywords ? `WHERE Name LIKE '%${keywords}%'` : ''}
  `)