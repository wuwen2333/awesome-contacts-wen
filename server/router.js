import Router from 'koa-router'
import { welcome, getContacts, getContactDetails } from './controller'

const router = new Router({
  prefix: '/api'
})

router.get('/', welcome)
router.get('/contact', getContacts)
router.get('/contact/:id', getContactDetails)

export default router;