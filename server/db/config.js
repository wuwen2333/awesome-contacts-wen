import env from '../../env.json';

const config = Object.assign({
  port: env.port,
  user: env.dbUser,
  database: env.dbName,
  host: env.dbHost,
  password: env.dbPassword,
  multipleStatements: true
})

export default config;
