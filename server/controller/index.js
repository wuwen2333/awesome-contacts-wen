import { queryContacts, queryContactsCount, queryContactDetails } from '../model'

export const welcome = async (ctx) => {
  ctx.body = {
    code: 0,
    data: null,
    message: 'Welcome to awesome-contacts server.'
  }
}

const contactColumns = ['UserID', 'Title', 'Name', 'BirthDate', 'IsFavorite'];
const sortTypes = ['ASC', 'DESC'];

export const getContacts = async (ctx) => {
  try {
    const {
      pageSize = 10,
      pageNumber = 0,
      keywords = '',
      sortType,
      sortProperty
    } = ctx.request.query
    const isSortTypeValid = sortType && sortTypes.includes(sortType);
    const isSortPropertyValid = sortProperty && contactColumns.includes(sortProperty);
    const contacts = await queryContacts({
      pageSize,
      pageNumber,
      keywords,
      sortType,
      sortProperty: isSortTypeValid && isSortPropertyValid && sortProperty
    });
    const total = await queryContactsCount({ keywords });

    ctx.body = {
      code: 0,
      data: {
        contacts,
        pageNumber,
        pageSize,
        total: total ? total[0]['COUNT(Name)'] : 0,
      },
      message: 'Get contacts successfully.'
    }
  } catch (err) {
    console.log(err.message);
    ctx.body = {
      code: 0,
      data: null,
      message: 'System Error.'
    }
  }
}

export const getContactDetails = async (ctx) => {
  try {
    const { id } = ctx.params;
    const contactDetails = await queryContactDetails(id);

    ctx.body = {
      code: 0,
      data: contactDetails,
      message: 'Get contact details successfully.'
    }
  } catch (err) {
    ctx.body = {
      code: 0,
      data: null,
      message: 'System Error.'
    }
  }
}
